# JSPuzzle

This project is a tile game.
The tile game shows an x number of tiles which needs to be placed in order depending on their number.

Achieved a website with Javascript, CSS and HTML capapble of: 
    Keeping current player's past scores (time it took for them to complete the game)
    Changing the number of tiles on every level
    Playing a sound every time a tile is moved and when the game is over
