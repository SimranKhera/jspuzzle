/* GLOBAL VARIABLES*/
var hour=0; var min=0; var sec=0; var count=0;
var numOfMoves=0;
var listPlayers=[];
var utilityObj;
var playerManagerObj;
var timer;
var pg;
function init(){
  utilityObj= new Utility();
  playerManagerObj= new PlayerManager();
  document.getElementById('playButtonId').addEventListener('click',mainProgram);
  document.getElementById('cancelButtonId').addEventListener('click',utilityObj.cancelPuzzlePlay);
  document.getElementById('levelSectionId').addEventListener('change',utilityObj.checkFormFilled);
}
/* 5 MAIN OBJECTS */

function Utility(){
  //Generates random number between min and max. Will be used by PuzzleGame.
  this.generateRandomNumber=function(min,max){
    return Math.floor(Math.random() * (max - min + 1) ) + min;
  }

  // Plays fireworks if input is true, otherwise plays beep sound
  this.playAudio= function(bool){

      if(bool){
        document.getElementById('fireworks').play();
      }else{
        document.getElementById('beep').play();
      }
  }
  //Used for testing
  this.sampleEasyBoardTests=function(){
      if(pg.puzzleWidth==3){
        return [ [1, 2, 3], [4, 0, 6], [7, 5, 8]];
      }else if(pg.puzzleWidth==4){
        return [[1,2,3,4],[5,6,7,8],[9,10,0,12],[13,14,11,15]];
      }else if(pg.puzzleWidth==5){
        return [[1,2,3,4,5],[6,7,8,9,10],[11,12,13,14,15],[16,17,0,19,20],[21,22,18,23,24]];
      }

  }

  this.showStats= function(){
      var name; var time; var moves;
      var table= document.getElementById('tableId');

      this.setUpTable(table);

      for(var i=0;i<listPlayers.length;++i){
        var newRow= table.insertRow();
        var nameCell= newRow.insertCell(0);
        var timeCell= newRow.insertCell(1);
        var movesCell= newRow.insertCell(2);
        var statusCell=newRow.insertCell(3);
        var levelCell=newRow.insertCell(4);
        nameCell.innerHTML=listPlayers[i].name;
        timeCell.innerHTML=listPlayers[i].duration;
        movesCell.innerHTML=listPlayers[i].numMoves;
        statusCell.innerHTML=listPlayers[i].theStatus;
        levelCell.innerHTML=listPlayers[i].dimension;
      }
  }

  //Table helper for stats, called from showStats
  this.setUpTable= function(table){
    table.innerHTML="";
    var newRow= table.insertRow();
    var nameCell= newRow.insertCell(0);
    var timeCell= newRow.insertCell(1);
    var movesCell= newRow.insertCell(2);
    var statusCell= newRow.insertCell(3);
    var levelCell= newRow.insertCell(4);
    nameCell.innerHTML="<b>Name</b>";
    timeCell.innerHTML="<b>Time (sec)</b>";
    movesCell.innerHTML="<b>Moves</b>";
    statusCell.innerHTML="<b>Status</b>";
    levelCell.innerHTML="<b>Level</b>"

  }
  //Resets game
  this.terminateGame= function(theStatus){
    document.getElementsByClassName('puzzleBoard')[0].removeEventListener('click',callProcessClickTile);
    clearInterval(timer);
    var playerName=document.getElementById('playerNameId').value;
    playerManagerObj.storeGameStats(theStatus,playerName,pg.puzzleWidth,numOfMoves,utilityObj.getTime());///////////////////////////////////

    numOfMoves=0;
    hour=0; min=0; sec=0; count=0;
    document.getElementById('playerNameId').value="";
    document.getElementById('secId').value=sec;
    document.getElementById('minId').value=min;
    document.getElementById('hourId').value=hour;
    document.getElementById('moveId').value=numOfMoves;
    document.getElementById('levelSectionId').selectedIndex=0;
    this.showStats();
  }

  //Calls terminateGame and disables the cancel button
  this.cancelPuzzlePlay= function(){
    utilityObj.terminateGame("cancelled");
    document.getElementById('cancelButtonId').disabled=true;
  }

  // If form is filled, enables play button and makes it green
  // otherwise, disables play button and makes it gray
  this.checkFormFilled= function(){
    var playerNameInput=document.getElementById('playerNameId');
    var levelSelect= document.getElementById('levelSectionId');

    if(playerNameInput.value.length!=0 & levelSelect.selectedIndex!=-1){
      document.getElementById('playButtonId').disabled=false;
      document.getElementById('playButtonId').style.background="green";
      document.getElementById('cancelButtonId').style.background="orange";
    }else{
      document.getElementById('playButtonId').disabled=true;
      document.getElementById('playButtonId').style.background="gray";
      document.getElementById('playButtonId').style.background="gray";
    }
  }

  //Updates clock every 1s--Chrono
  this.showChrono=function(){
    count++;
    sec=count;
    if(sec>59){sec=0;min++;count=0;}
    if(min>59){hour++;}
    document.getElementById('secId').value=sec;
    document.getElementById('minId').value=min;
    document.getElementById('hourId').value=hour;
    timer= setTimeout("utilityObj.showChrono()",1000);
  }

  //Returns total time in seconds to display as stats in stats table
  this.getTime= function(){
    var hours=parseInt(document.getElementById('hourId').value);
    var mins= parseInt(document.getElementById('minId').value);
    var secs=parseInt(document.getElementById('secId').value);
    return (hours*3600)+(mins*60)+secs;
  }
}//END OF UTILITY OBJECT

function PlayerManager(){

  this.storeGameStats= function(theStatus,name,dimension,moves,time){
    var p= new Player(theStatus,name,dimension,moves,time);
    listPlayers.push(p);
  }
} //END OF PLAYERMANAGER OBJECT

function Player(theStatus,name,dimension,moves,time){
  this.theStatus= theStatus;
  this.name=name;
  this.dimension=dimension;
  this.numMoves= moves;
  this.duration= time;
}//END OF PLAYER OBJECT

function Tile(row,col,tileType, indexNum){
  this.row=row;
  this.col=col;
  this.tileType=tileType;//true if is filled, false if empty
  this.tileNum=indexNum;//the number of the tile
}

function PuzzleGame(puzzleWidth){
  this.puzzleWidth=puzzleWidth;//3,4 or 5
  this.puzzleBoard=new Array(puzzleWidth);
  this.goalState= new Array(puzzleWidth);

  this.createGoalState=function(){

    for(var i=0;i<this.goalState.length;++i){
      this.goalState[i]= new Array(this.puzzleWidth);
    }
    //Assigning values
    var tileNum=1;
    for(var i=0;i<this.goalState.length;++i){
      for(var j=0;j<this.goalState[i].length;++j){
        if(i!=this.puzzleWidth-1 || j!=this.puzzleWidth-1){
          this.goalState[i][j]=new Tile(i,j,true,tileNum);
          tileNum++;
        }else{
          tileNum=0;
          this.goalState[i][j]=new Tile(i,j,false,tileNum);
        }
      }
    }//end of floop
  }//end of createGoalState

  this.createBoardStructure= function(){
    //Creates 2d array from 1d array
    for(var i=0;i<this.puzzleBoard.length;++i){
      this.puzzleBoard[i]= new Array(this.puzzleWidth);
    }
    //put start comment here if you want to use test cases instead:
    var max=(this.puzzleWidth*this.puzzleWidth)-1;//max num of tile

    //Set random values
    for(var i=0;i<this.puzzleBoard.length;++i){
      for(var j=0;j<this.puzzleBoard[i].length;++j){
        var num=utilityObj.generateRandomNumber(0,max);

        //loops to make sure there's no duplicate tile
        while(checkRepitition(this.puzzleBoard,num)){
          var num=utilityObj.generateRandomNumber(0,max);
        }
        //creates tile objs according to tile num
        if(num==0){
          this.puzzleBoard[i][j]=new Tile(i,j,false,num);
        }else{
          this.puzzleBoard[i][j]=new Tile(i,j,true,num);
        }
      }
    }//end of floop


    //THIS IS USED FOR TESTING, ONE CAN USE IT TO HEAR THE FIREWORKS
    //IT WORKS FOR ALL LEVELS
    /*var arr=utilityObj.sampleEasyBoardTests();
    for(var i=0;i<arr.length;i++){
      for(var j=0;j<arr[i].length;++j){
        if(arr[i][j]==0){
          this.puzzleBoard[i][j]=new Tile(i,j,false,arr[i][j]);
        }else{
          this.puzzleBoard[i][j]=new Tile(i,j,true,arr[i][j]);
        }
      }
    }*/
  }//end of createBoardStructure

  //Loops through the puzzleBoard and draws on screen
  this.drawPuzzleBoard=function(){
    this.removeAllTiles();
    for(var i=0;i<this.puzzleBoard.length;++i){
      for(var j=0;j<this.puzzleBoard[i].length;++j){
        var newTile= document.createElement('DIV');
        newTile.innerHTML="<p>"+this.puzzleBoard[i][j].tileNum+"</p>";
        //assigning classes
        if(this.puzzleBoard[i][j].tileType){
          newTile.className="filedTile";
        }else{
          newTile.className="emptyTile";
        }
        var divHeight=document.getElementById('puzzleBoard').clientHeight;
        var divWidth=document.getElementById('puzzleBoard').clientWidth;

        newTile.style.width=divWidth/this.puzzleWidth+"px";
        newTile.style.height=divHeight/this.puzzleWidth+"px";
        document.getElementById('row'+i).appendChild(newTile);
      }
    }
  }//end of drawPuzzleBoard

  //called by drawPuzzleBoard, clears the puzzle
  this.removeAllTiles= function(){
    document.getElementById('row0').innerHTML="";
    document.getElementById('row1').innerHTML="";
    document.getElementById('row2').innerHTML="";
    document.getElementById('row3').innerHTML="";
    document.getElementById('row4').innerHTML="";

  }
  //Swaps two tiles, calls helper method that returns the index of tiles(array)
  this.swap2Tiles= function(tileNum1,tileNum2){
    var tile1Position=this.getTileIndex(tileNum1);//[0]=i,[1]=j
    var tile2Position=this.getTileIndex(tileNum2);//[0]=i,[1]=j
    //swapping in array
    var temp=this.puzzleBoard[tile1Position[0]][tile1Position[1]];
    this.puzzleBoard[tile1Position[0]][tile1Position[1]]=this.puzzleBoard[tile2Position[0]][tile2Position[1]];
    this.puzzleBoard[tile2Position[0]][tile2Position[1]]=temp;
  }

  //returns true if goal is reached
  this.match2States= function(state1,state2){
    for(var i=0;i<state1.length;++i){
      for(var j=0;j<state1[i].length;++j){
        if(state1[i][j].tileNum!=state2[i][j].tileNum){
          return false;
        }
      }
    }
    return true;
  }

  //returns neighbhours
  this.getNeighboursIndicesArr= function(tileNumber){
    var tilePosition=this.getTileIndex(tileNumber);
    var left; var top; var right; var bottom;
    //Row and column of current tile
    var row=tilePosition[0]; var col=tilePosition[1];

    if(col-1>=0){ left=this.puzzleBoard[row][col-1].tileNum; }else{ left=-1; }//left
    if(row-1>=0){top=this.puzzleBoard[row-1][col].tileNum;}else{top=-1;}//top
    if(col+1<=this.puzzleWidth-1){right=this.puzzleBoard[row][col+1].tileNum;}else{right=-1;}//right
    if(row+1<=this.puzzleWidth-1){bottom=this.puzzleBoard[row+1][col].tileNum;}else{bottom=-1;}//bottom
    return [left,top,right,bottom];
  }

  //runs every time tile is clicked, checks if there's a win
  this.processClickTile= function(tileNum){
    var neighbours= pg.getNeighboursIndicesArr(tileNum);
    for(var i=0;i<neighbours.length;++i){
      if(neighbours[i]==0){
        this.swap2Tiles(tileNum,neighbours[i]);
        numOfMoves++;
        document.getElementById('moveId').value=numOfMoves;
        this.drawPuzzleBoard();
        if(this.match2States(this.puzzleBoard,this.goalState)){
          utilityObj.playAudio(true);
          utilityObj.terminateGame("success");
        }else{
            utilityObj.playAudio(false);
        }
      }
    }
  }
  //Helper which returns the index (in an array) of the given tilenumber
  //Is called by swap2Tiles
  this.getTileIndex=function(tileNumber){
    for(var i=0;i<this.puzzleBoard.length;i++){
      for(var j=0;j<this.puzzleBoard[i].length;++j){
        if(this.puzzleBoard[i][j].tileNum==tileNumber){
          return [i,j];
        }
      }
    }
  }
}//END OF PUZZLEGAME

//runs when play button is clicked.Creates necessary objs
function mainProgram(){
  document.getElementById('cancelButtonId').disabled=false;
  document.getElementsByClassName('puzzleBoard')[0].addEventListener('click',callProcessClickTile);
  puzzleWidth=parseInt(document.getElementById('levelSectionId').value);
  pg= new PuzzleGame(puzzleWidth);
  pg.createBoardStructure();
  pg.createGoalState();
  pg.drawPuzzleBoard();
  utilityObj.showChrono();
}
/*  HELPER  */
//Returns true if num already exists in given array
//called by createBoardStructure (when generating a random board)
function checkRepitition(arr,num){
  for(var i=0;i<arr.length;i++){
    for(var j=0;j<arr[i].length;++j){
      if(typeof(arr[i][j])!=='undefined'){
        if(arr[i][j].tileNum==num){
          return true;
        }
      }
    }
  }
  return false;
}
//calls processClickTile
function callProcessClickTile(event){
  if(event.target.tagName=='DIV'){
    var tileNum=parseInt(event.target.children[0].textContent);
  }else{
    var tileNum=parseInt(event.target.textContent);
  }
  pg.processClickTile(tileNum);
}
document.addEventListener('DOMContentLoaded',init);
